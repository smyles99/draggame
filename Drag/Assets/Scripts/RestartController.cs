﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartController : MonoBehaviour
{
    public static bool isRestart;
    public Image img;

    // Start is called before the first frame update
    void Start()
    {
        isRestart = false;
        img.gameObject.SetActive(false);
    }
    
    public void Restart()
    {
        isRestart = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isRestart == true)
        {
            SceneManager.LoadScene("SampleScene");
        }
        if (StrikeCounter.GameOver == true)
        {
            img.gameObject.SetActive(true);
        }
    }
}
