﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Time : MonoBehaviour
{
    public static float currentTime = 0f;
    float startingTime = 0f;

    [SerializeField] Text countdownText;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += 1 * UnityEngine.Time.deltaTime;
        countdownText.text = currentTime.ToString("0");

        
    }
}
