﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorR : MonoBehaviour
{
    public AudioSource ping;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.currentTime > 15)
        {
            rb.gravityScale = 0.17f;
            
        }
        if (Time.currentTime > 30)
        {
            rb.gravityScale = 0.2f;

        }
        if (Time.currentTime > 45)
        {
            rb.gravityScale = 0.23f;

        }
        if (Time.currentTime > 60)
        {
            rb.gravityScale = 0.26f;

        }
        if (Time.currentTime > 75)
        {
            rb.gravityScale = 0.29f;

        }
        if (Time.currentTime > 90)
        {
            rb.gravityScale = 0.32f;

        }
        if (Time.currentTime > 105)
        {
            rb.gravityScale = 0.35f;

        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.CompareTag("Red") && other.gameObject.CompareTag("RBlock"))
        {
            Destroy(gameObject);
            Score.score += 20;
            ping.Play();
        }
    }
}