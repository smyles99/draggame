﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StrikeCounter : MonoBehaviour
{
    public static bool GameOver;
    public static int lives;

    public SpriteRenderer strike1;
    public SpriteRenderer strike2;
    public SpriteRenderer strike3;

    public AudioSource strike;

    [SerializeField] Text gameOverText;
    // Start is called before the first frame update
    void Start()
    {
        strike1.gameObject.SetActive(false);
        strike2.gameObject.SetActive(false);
        strike3.gameObject.SetActive(false);
        lives = 3;
        GameOver = false;

        gameOverText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (lives == 2)
        {
            strike1.gameObject.SetActive(true);
            //strike.Play();
            //Destroy(strike);
        }
        if (lives == 1)
        {
            strike2.gameObject.SetActive(true);
            //strike.Play();
        }
        if (lives == 0)
        {
            strike3.gameObject.SetActive(true);

            //strike.Play();
            GameOver = true;
            gameOverText.gameObject.SetActive(true);
            

            if( Score.score > PlayerPrefs.GetInt("highscore"))
            {
                PlayerPrefs.SetInt("highscore", Score.score);
            }
        }

    }
}
