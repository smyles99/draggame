﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    public static bool mouseDown = false;
    private float startPosX;
    private float startPosY;
    private bool isBeingHeld = false;

    


    private void Start()
    {
        
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {


            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - this.transform.localPosition.x;
            startPosX = mousePos.y - this.transform.localPosition.y;

            isBeingHeld = true;
            mouseDown = true;
        }

    }

    private void OnMouseUp()
    {
        isBeingHeld = false;
        mouseDown = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (isBeingHeld == true)
        {

            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            this.gameObject.transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, 0);
        }
        if (transform.position.y < -6f)
        {
            Destroy(gameObject);
            StrikeCounter.lives -= 1;
        }
        if (StrikeCounter.GameOver == true)
        {
            isBeingHeld = false;
        }

    }
}
