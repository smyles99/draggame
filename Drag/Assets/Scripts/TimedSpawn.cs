﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawn : MonoBehaviour
{
    public GameObject spawnee;
    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDelay;
    

    public int xPos;
    public int yPos;
    // Start is called before the first frame update
    void Start()
    {
        
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    public void SpawnObject()
    {
        xPos = Random.Range(-6, 6);
        yPos = Random.Range(-2, 3);

        Instantiate(spawnee, new Vector3(xPos,yPos), Quaternion.identity);
        //transform.rotation
        if (stopSpawning)
        {
            CancelInvoke("SpawnObject");
        }
    }
    private void Update()
    {
        if (Time.currentTime > 10)
        {
            //print("2");
        }
    }
}



