﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreText;
    public Text highScoreText;
    public static int score;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        highScoreText.text = "High Score: " + PlayerPrefs.GetInt("highscore");
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText();
    }
    void ScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }
}
