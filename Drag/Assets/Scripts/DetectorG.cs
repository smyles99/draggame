﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorG : MonoBehaviour
{
    public AudioSource ping;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.currentTime > 15)
        {
            rb.gravityScale = 0.12f;
        }
        if (Time.currentTime > 30)
        {
            rb.gravityScale = 0.15f;

        }
        if (Time.currentTime > 45)
        {
            rb.gravityScale = 0.18f;

        }
        if (Time.currentTime > 60)
        {
            rb.gravityScale = 0.21f;

        }
        if (Time.currentTime > 75)
        {
            rb.gravityScale = 0.24f;

        }
        if (Time.currentTime > 90)
        {
            rb.gravityScale = 0.27f;

        }
        if (Time.currentTime > 105)
        {
            rb.gravityScale = 0.3f;

        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.CompareTag("Green") && other.gameObject.CompareTag("GBlock"))
        {
            Destroy(gameObject);
            Score.score += 15;
            ping.Play();
        }
    }
}