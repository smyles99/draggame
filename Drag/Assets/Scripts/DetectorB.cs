﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorB : MonoBehaviour
{
    public AudioSource ping;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        print(rb.gravityScale);

        if (Time.currentTime > 15)
        {
            rb.gravityScale = 0.09f;
            
        }
        if (Time.currentTime > 30)
        {
            rb.gravityScale = 0.12f;

        }
        if (Time.currentTime > 45)
        {
            rb.gravityScale = 0.15f;

        }
        if (Time.currentTime > 60)
        {
            rb.gravityScale = 0.15f;

        }
        if (Time.currentTime > 75)
        {
            rb.gravityScale = 0.18f;

        }
        if (Time.currentTime > 90)
        {
            rb.gravityScale = 0.21f;

        }
        if (Time.currentTime > 105)
        {
            rb.gravityScale = 0.24f;

        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.CompareTag("Blue") && other.gameObject.CompareTag("BBlock"))
        {
            Destroy(gameObject);
            Score.score += 10;
            ping.Play();
        }
        
    }
}
